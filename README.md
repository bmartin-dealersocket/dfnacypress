# DealerFire New Accounts Cypress Tester

---

To run this tester, you will need the following things installed on your computer:

1. Node.js and npm, available here: **[link](https://nodejs.org/en/download/)**
    a. Not sure if you have Node.js? If you run the tester and get the following error, your Node installation is not live: *'npm' is not recognized as an internal or exteral command, operable program or batch file.*
2. Chrome, of course
3. Windows machine. The test runner uses a .bat file to execute the test, so a Mac or Linux machine will have to run the test runner manually. Just use Windows.
4. To create tests, you'll need Bitbucket access to commit to this repository. Message me if you need more information on that. Everybody has access to run the tester.

## Download tester

You can download the tester files directly, or clone from Sourcetree. To download, go [here](https://bitbucket.org/bmartin-dealersocket/dfnacypress/downloads/), and then extract the zip folder somewhere on your computer where you can keep track of it. To clone, do so in SourceTree or VS Code.

## Run Tester

Once you've downloaded the tester, click into the folder on your computer and double-click the **exec.bat** file. Fill out the site URL you want to test and hit enter.

*The first time you run this, it will download a copy of Chromium. The next time you run it, it won't download this and will run much faster.*

Once the tester has finished, double-click on the **report** folder and open the **report.html** file. Fix any failure items in the test, then run the test again. Once all tests pass, screenshot this file and attach to your Salesforce project.
