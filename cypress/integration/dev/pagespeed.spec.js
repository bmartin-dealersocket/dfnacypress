/// <reference types="cypress" />

describe('Performance Metrics for ' + Cypress.env('url'), () => {
    before(() => {
        cy.visit(Cypress.env('url'));
    });
    it('Homepage loads in under 5 seconds', function () {
        cy.lighthouse({
            performance: 1,
            'first-meaningful-paint': 5000, // homepage should load in 5 seconds
        });
    })
});
