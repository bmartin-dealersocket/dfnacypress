/// <reference types="cypress" />

describe('SRP - ' + Cypress.env('url') + '/inventory', () => {
    before(() => {
        cy.visit(Cypress.env('url') + '/inventory');
    });
    it('SRP should have one and only one h1 tag', function () {
        cy.get('h1')
            .should('exist')
            .and('have.length', 1);
    })
});
