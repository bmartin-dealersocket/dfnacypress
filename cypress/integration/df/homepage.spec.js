/// <reference types="cypress" />

describe('Homepage - ' + Cypress.env('url'), () => {
    before(() => {
        cy.visit(Cypress.env('url'));
    });
    it('Homepage should have one and only one h1 tag', function () {
        cy.get('h1')
            .should('exist')
            .and('have.length', 1);
    });
});
