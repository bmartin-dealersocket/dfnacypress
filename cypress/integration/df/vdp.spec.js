/// <reference types="cypress" />

describe('VDP - ' + Cypress.env('url'), () => {
    before(() => {
        cy.visit(Cypress.env('url') + '/inventory');
        cy.get('.js-vehicle-item-link:first').click();
    });
    it('VDP should have one and only one h1 tag', function () {
        cy.get('h1')
            .should('exist')
            .and('have.length', 1);
    })
});
