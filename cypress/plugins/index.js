/// <reference types="cypress" />
const { lighthouse, prepareAudit } = require('cypress-audit');

module.exports = (on, config) => {
  on('before:browser:launch', (broswer = {}, launchOptions) => {
    prepareAudit(launchOptions);
  })
  on('task', {
    lighthouse: lighthouse(),
  });
}
