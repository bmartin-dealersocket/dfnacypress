// Import commands.js using ES2015 syntax:
import './commands'
import 'cypress-mochawesome-reporter/register';

import addContext from 'mochawesome/addContext';

Cypress.on('test:after:run', (test, runnable) => {
    // addContext({ test }, JSON.stringify(test));
});

// Alternatively you can use CommonJS syntax:
// require('./commands')

// Supress all uncaught exceptions
Cypress.on('uncaught:exception', (err, runnable) => {
    // ? use this line if I need to output uncaught exceptions to the report
    // expect(err.message).to.include('error something something');

    // * returning false here prevents Cypress from failing the test
    return false
});
