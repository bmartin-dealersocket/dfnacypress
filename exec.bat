@echo off
setlocal

set /P CYPRESS_BASE_URL="What is the URL to test (including http/https://)?  "

echo === === === ===   Testing %CYPRESS_BASE_URL%.
echo.

echo === === === ===   Check npm has required packages
call npm install

echo === === === ===   Pull site links from sitemap page, and save for cypress to use
echo === === === ===   IF THE PAGE IS NOT ON THE SITEMAP, IT WILL NOT BE TESTED
call node scrape-urls.js

echo === === === ===   Check Bitbucket for updates to the tests
call node update-tests.js

echo === === === ===   Clean up previous test report, if needed
if exist "./cypress/reports/" rmdir "./cypress/reports/" /s /q
mkdir "./cypress/reports"

TIMEOUT 3

echo === === === ===   Start test
call npm run test

echo === === === ===   Prep report
call npm run combine-reports
call npm run generate-report
@REM call npm run copy-report

echo.
echo.
echo === === === ===   Testing %CYPRESS_BASE_URL% finished
echo === === === ===   Test report stored in df-cypress-build/reports/report.html
echo.
echo.
echo === === === ===   Bye
echo.
pause

endlocal
exit
