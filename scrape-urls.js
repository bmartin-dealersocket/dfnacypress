const fetch = require('node-fetch');
const fs = require('fs');
const { parse } = require('node-html-parser');
// remove old .json file with site links

function isURL(str) {
    var urlRegex = '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$';
    var url = new RegExp(urlRegex, 'i');
    return str.length < 2083 && url.test(str);
}

// verify URL is valid
var url = process.env.CYPRESS_BASE_URL;
if (url.indexOf('.com/') === -1) url += '/';
if (!isURL(url)) fs.writeFile('./cypress-links.json', 'Invalid URL', err => {
    if (err) throw err;
    return err;
});

// fetch sitemap HTML
fetch(url + 'sitemap')
    .then(response => response.text())
    .then(data => {
        var html = parse(data);
        // pull all links from the .mod-sitemap element
        var links = [];
        html.querySelectorAll('.mod-sitemap a').forEach(a => {
            var href = a.getAttribute('href');
            // ? skip this site link if home, SRP, or VDP
            // var exclusions = ['/', '/inventory', 'vehicle-details'];
            // if (!exclusions.includes(href)) links.push(`\'${a.getAttribute('href')}\'`);
        });
        // remove duplicates from the array
        links = [...new Set(links)];
        // write the links to a .json file for use in cypress
        fs.writeFile('./cypress-links.txt', `[${links}]`, err => {
            if (err) throw err;
            return err;
        });
    })
    .catch(err => {
        if (err) throw err;
        return err;
    })
