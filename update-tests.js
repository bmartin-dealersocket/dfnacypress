const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');

// remove old test files
const dir = './cypress/integration/df';
fs.readdir(dir, (err,files) => {
    if (err) throw err;
    for (const file of files) {
        fs.unlink(path.join(dir, file), err => {
            if (err) throw err;
        })
    }
});

// get list of tests in repo
fetch('https://api.bitbucket.org/2.0/repositories/bmartin-dealersocket/dfnacypress/src/master/cypress/integration/df/')
    .then(response => response.json())
    .then(tests => {
        for (const test of tests.values) {
            // for each test, create spec file with test content
            console.log('Update ' + test.escaped_path);
            fetch(test.links.self.href)
                .then(response => response.text())
                .then(data => {
                    fs.writeFile(test.escaped_path, data, err => {
                        if (err) throw err;
                    })
                })
                .catch(err => {
                    if (err) throw err;
                });
        }
        console.log('Test spec files updated');
    })
    .catch(err => console.error(err));
